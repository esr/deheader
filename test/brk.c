/*
 * Items: brk(
 * Standardized-By: SuS
 * Detected-by: gcc-4.4.3 + Linux
 */

#include <unistd.h>

int main(int arg, char **argv)
{
    brk(NULL);
}
